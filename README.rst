===============================
kosmos
===============================

OpenStack Global Server Load Balancing

* Free software: Apache license
* Source: http://git.openstack.org/cgit/openstack/kosmos
* Bugs: http://bugs.launchpad.net/kosmos
