============
Installation
============

At the command line::

    $ pip install kosmos

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv kosmos
    $ pip install kosmos
